let assert = require('assert');

/**
 * Clase Portafolio
 */
class Portafolio {
    constructor() {
        this.moneys = [];
    }

    add(...moneys) {
        this.moneys = this.moneys.concat(moneys);
    }

    evaluate(currency) {
        let total = this.moneys.reduce((sum, money) => {
            return sum + money.amount;
        }, 0);

        return new Money(total, currency)
    }
}

class Money { 
    constructor(amount, currency) {
        this.amount = amount;
        this.currency = currency;
    }
}

//Cuenta/Portafolio que necesita o en la que pueda almacenar dinero
let fifteenDollar = new Money(15, "USD");

let fiveDollar = new Money(5, "USD");

let tenDollar = new Money(10, "USD");

let portafolio = new Portafolio();

portafolio.add(fiveDollar, tenDollar);

assert.deepStrictEqual(portafolio.evaluate("USD"), fifteenDollar);

